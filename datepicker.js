/**
 *  DatePicker plugin
 *
 *  2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @modifier  Mantas <mantas@whatagraph.com>
 *  @copyright Copyright (c) 2016, Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://solutera.lt
 *
 *  @note      Under Construction :)
 *
 */

(function(window, $) {
    'use strict';

    Date.prototype.getWeekNumber = function()
    {
        var d = new Date(+this);
        d.setHours(0, 0, 0, 0);
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        return Math.ceil((((d-new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
    };

    Date.prototype.addDays = function(days)
    {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };

    var DatePickerMode = {
        Day: 'day',
        Week: 'week'
    };

    var DatePickerWeekOffset =
        {
            Sunday : 0,
            Monday : 1,
            Tuesday : 2,
            Wednesday : 3,
            Thursday : 4,
            Friday : 5,
            Saturday : 6
        };

    var Calendar = function() {
        this.viewDate = new Date(); // Current view date of calendar
    };

    var DatePicker = function(el, options) {
        this.now = new Date();
        this.now.setHours(0, 0, 0, 0);

        // Default settings:
        this.defaults = {
            mode: DatePickerMode.Day,
            weekOffset: DatePickerWeekOffset.Monday,
            maxDate : null,
            minDate : null,
            selectedFrom: null,
            selectedTill: null,
            locale: {
                days: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                week: 'Week'
            },
            onDateSelected : function(from, till) { console.log('Date selected: ', from, till); }
        };

        // Properties
        this.$el = $(el);
        this.options = options;
        this.$container = null;
        this.$daysView = null;
        this.$monthsView = null;
        this.$yearsView = null;
        this.$header = null;
        this.selectedFrom = null;
        this.selectedTill = null;
        this.calendar = new Calendar();    // Current calendar
        this.calendar.viewDate = new Date(this.now.getFullYear(), this.now.getMonth(), 1);

        this.tpl =
            '<div class="date-picker-container">' +
            '   <table class="header">' +
            '       <thead>' +
            '           <tr class="controls">' +
            '               <th class="col-prev"><a href="javascript:" class="action-prev"><span>Previous</span></a></th>' +
            '               <th class="col-month"><a href="javascript:" class="action-month"><span>Month</span></a></th>' +
            '               <th class="col-year"><a href="javascript:" class="action-year"><span>Year</span></a></th>' +
            '               <th class="col-next"><a href="javascript:" class="action-next"><span>Next</span></a></th>' +
            '            </tr>' +
            '       </thead>' +
            '   </table>' +
            '' +
            '   <table class="calendar-view-table days-view">' +
            '       <thead>' +
            '            <tr class="labels">' +
            '                <th class="week-cell"><span>{$locale[week]}</span></th>' +
            '                <th class="day-cell day-0"><span>{$dayName[0]}</span></th>' +
            '                <th class="day-cell day-1"><span>{$dayName[1]}</span></th>' +
            '                <th class="day-cell day-2"><span>{$dayName[2]}</span></th>' +
            '                <th class="day-cell day-3"><span>{$dayName[3]}</span></th>' +
            '                <th class="day-cell day-4"><span>{$dayName[4]}</span></th>' +
            '                <th class="day-cell day-5"><span>{$dayName[5]}</span></th>' +
            '                <th class="day-cell day-6"><span>{$dayName[6]}</span></th>' +
            '            </tr>' +
            '        </thead>' +
            '        <tbody>' +
            '           <tr class="week-row">' +
            '               <th class="week-cell"><span>{w}</span></th>' +
            '               <td class="day-cell day-0"><span>{d}</span></td>' +
            '               <td class="day-cell day-1"><span>{d}</span</td>' +
            '               <td class="day-cell day-2"><span>{d}</span</td>' +
            '               <td class="day-cell day-3"><span>{d}</span></td>' +
            '               <td class="day-cell day-4"><span>{d}</span</td>' +
            '               <td class="day-cell day-5"><span>{d}</span</td>' +
            '               <td class="day-cell day-6"><span>{d}</span</td>' +
            '           </tr>' +
            '        </tbody>' +
            '    </table>' +
            '' +
            '   <table class="calendar-view-table months-view">' +
            '       <tr>' +
            '           <td class="month-cell month-0"><span>{m}</span></td>' +
            '           <td class="month-cell month-1"><span>{m}</span></td>' +
            '           <td class="month-cell month-2"><span>{m}</span></td>' +
            '           <td class="month-cell month-3"><span>{m}</span></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td class="month-cell month-4"><span>{m}</span></td>' +
            '           <td class="month-cell month-5"><span>{m}</span></td>' +
            '           <td class="month-cell month-6"><span>{m}</span></td>' +
            '           <td class="month-cell month-7"><span>{m}</span></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td class="month-cell month-8"><span>{m}</span></td>' +
            '           <td class="month-cell month-9"><span>{m}</span></td>' +
            '           <td class="month-cell month-10"><span>{m}</span></td>' +
            '           <td class="month-cell month-11"><span>{m}</span></td>' +
            '       </tr>' +
            '   </table>' +
            '' +
            '   <table class="calendar-view-table years-view">' +
            '       <tr>' +
            '           <td class="year-cell year-0"><span>{y}</span></td>' +
            '           <td class="year-cell year-1"><span>{y}</span></td>' +
            '           <td class="year-cell year-2"><span>{y}</span></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td class="year-cell year-3"><span>{y}</span></td>' +
            '           <td class="year-cell year-4"><span>{y}</span></td>' +
            '           <td class="year-cell year-5"><span>{y}</span></td>' +
            '       </tr>' +
            '       <tr>' +
            '           <td class="year-cell year-6"><span>{y}</span></td>' +
            '           <td class="year-cell year-7"><span>{y}</span></td>' +
            '           <td class="year-cell year-8"><span>{y}</span></td>' +
            '       </tr>' +
            '   </table>' +
            '</div>';

        // Initialize
        this.init = function()
        {
            // Combine with user settings
            this.options = $.extend({}, this.defaults, this.options);

            this.$container = $(this.tpl);
            this.$container.appendTo(this.$el);
            this.$daysView = this.$container.find('.days-view');
            this.$header = this.$container.find('.header');
            this.$monthsView = this.$container.find('.months-view');
            this.$yearsView = this.$container.find('.years-view');

            this.selectedFrom = this.options.selectedFrom;
            this.selectedTill = this.options.selectedTill;

            // Set labels
            var $labels = this.$daysView.find('.labels');
            $labels.find('.week-cell span').text(this.options.locale.week);

            for (var d = 0; d < 7; d++)
            {
                var names = this.options.locale.days;
                var name = names[(d + this.options.weekOffset) % 7];
                $labels.find('.day-' + d + ' span').text(name);
            }

            // Set week rows
            var $weekRowsContainer = this.$daysView.find('tbody');
            var $weekRowTpl = $weekRowsContainer.find('.week-row');

            $weekRowTpl.detach();

            for (var r = 0; r < 6; r++)
            {
                var $weekRow = $weekRowTpl.clone();
                $weekRow.addClass('week-row-' + r);
                $weekRow.appendTo($weekRowsContainer);
            }

            // Set month names
            for (var m = 0; m < 12; m++)
            {
                var $monthCell = this.$monthsView.find('.month-' + m);
                $monthCell.find('span').text(this.options.locale.months[m].slice(0, 3));
                $monthCell.data('month', m);
            }

            this.$monthsView.hide();
            this.$yearsView.hide();

            // Bind actions
            $weekRowsContainer.find('.week-cell').bind('click', $.proxy(this.selectWeek, this));
            $weekRowsContainer.find('.day-cell').bind('click', $.proxy(this.selectDay, this));
            this.$header.find('.action-next').bind('click', $.proxy(this.actionNext, this));
            this.$header.find('.action-prev').bind('click', $.proxy(this.actionPrevious, this));
            this.$header.find('.action-month').bind('click', $.proxy(this.actionToggleMonthView, this));
            this.$header.find('.action-year').bind('click', $.proxy(this.actionToggleYearView, this));
            this.$monthsView.find('.month-cell').bind('click', $.proxy(this.selectMonth, this));
            this.$yearsView.find('.year-cell').bind('click', $.proxy(this.selectYear, this));

            this.render();
            return this;
        };

        /**
         * @param {Date} from
         * @param {Date} [till]
         */
        this.selectDate = function(from, till)
        {
            // Clear time
            from.setHours(0,0,0,0);
            if (till)
                till.setHours(0,0,0,0);

            this.selectedFrom = from;
            this.selectedTill = (till != undefined) ? till : from;
            this.render();
        };

        /**
         * @param {Date} date
         */
        this.showDate = function(date)
        {
            date.setHours(0,0,0,0);
            this.calendar.viewDate = date;
            this.render();
        };

        this.actionNext = function()
        {
            this.calendar.viewDate.setMonth(this.calendar.viewDate.getMonth() + 1);
            this.render();
        };

        this.actionPrevious = function()
        {
            this.calendar.viewDate.setMonth(this.calendar.viewDate.getMonth() - 1);
            this.render();
        };

        this.actionToggleMonthView = function()
        {
            if (this.$monthsView.is(":visible"))
            {
                this.$monthsView.hide();
                this.$yearsView.hide();
                this.$daysView.show();
            }
            else
            {
                this.$monthsView.show();
                this.$yearsView.hide();
                this.$daysView.hide();
            }
        };

        this.actionToggleYearView = function()
        {
            if (this.$yearsView.is(":visible"))
            {
                this.$yearsView.hide();
                this.$monthsView.hide();
                this.$daysView.show();
            }
            else
            {
                this.$yearsView.show();
                this.$monthsView.hide();
                this.$daysView.hide();
            }
        };

        this.selectYear = function(e)
        {
            var $cell = $(e.currentTarget);
            var year = $cell.data('year');
            this.calendar.viewDate.setFullYear(year);
            this.actionToggleYearView();
            this.render();
        };

        this.selectMonth = function(e)
        {
            var $cell = $(e.currentTarget);
            var month = $cell.data('month');
            this.calendar.viewDate.setMonth(month);
            this.actionToggleMonthView();
            this.render();
        };

        this.selectDay = function(e)
        {
            var $cell = $(e.currentTarget);
            var date = $cell.data('date');

            if ($cell.hasClass('disabled'))
                return;

            this.calendar.viewDate = new Date(date.getFullYear(), date.getMonth(), 1);

            switch (this.options.mode)
            {
                case DatePickerMode.Day:
                {
                    this.selectedFrom = this.selectedTill = date;
                    break;
                }
                case DatePickerMode.Week:
                {
                    date = date.addDays(-this.options.weekOffset);
                    var weekStartDate = date.addDays(-date.getDay() + this.options.weekOffset);
                    this.selectedFrom = weekStartDate;
                    this.selectedTill = weekStartDate.addDays(6);
                    break;
                }
            }

            this.render();

            this.options.onDateSelected(this.selectedFrom, this.selectedTill);
        };

        this.selectWeek = function(e) {
            var $cell = $(e.currentTarget);
            var date = $cell.data('date');

            if ($cell.hasClass('disabled'))
                return;

            this.selectedFrom = date;
            this.selectedTill = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 6);

            this.render();

            this.options.onDateSelected(this.selectedFrom, this.selectedTill);
        };

        this.render = function() {
            this.renderDaysView();
        };

        this.getMonthClass = function(currentDate, otherDate)
        {
            var currentMonth = currentDate.getMonth() + 1;
            var otherMonth = otherDate.getMonth() + 1;

            if (currentMonth == otherMonth)
                return 'month-curr';

            if ((currentMonth == 12 && otherMonth == 1) || (currentMonth < otherMonth && !(currentMonth == 1 && otherMonth == 12)))
                return 'month-next';
            else
                return 'month-prev';
        };

        this.renderDaysView = function()
        {
            var monthStartDate = new Date(this.calendar.viewDate.getFullYear(), this.calendar.viewDate.getMonth(), 1);

            // Remove state classes
            this.$daysView.find('.week-row, .day-cell, .week-cell').removeClass('selected day-curr week-curr month-prev month-next month-curr disabled');
            this.$monthsView.find('.month').removeClass('selected');
            this.$yearsView.find('.year').removeClass('selected');

            var dayOffset = this.options.weekOffset;
            for (var r = 0; r < 6; r++)
            {
                var wd = monthStartDate.getDay();
                var weekStartDate = monthStartDate.addDays(-wd + dayOffset);

                var $weekRow = this.$daysView.find('.week-row-' + r);
                var $weekCell = $weekRow.find('.week-cell');
                $weekCell.find('span').text(weekStartDate.getWeekNumber());
                $weekCell.data('date', weekStartDate);

                $weekRow.addClass(this.getMonthClass(this.calendar.viewDate, weekStartDate));

                if (this.now.getFullYear() == weekStartDate.getFullYear() && this.now.getWeekNumber() == weekStartDate.getWeekNumber())
                    $weekRow.addClass('week-curr');

                for (var d = 0; d < 7; d++)
                {
                    var date = weekStartDate.addDays(d);

                    var $dayCell = $weekRow.find('.day-' + d);
                    $dayCell.find('span').text(date.getDate());
                    $dayCell.data('date', date);

                    if (this.now.getFullYear() == date.getFullYear() && this.now.getMonth() == date.getMonth() && this.now.getDate() == date.getDate())
                        $dayCell.addClass('day-curr');

                    if (this.selectedFrom && this.selectedFrom.getTime() <= date.getTime() && date.getTime() <= this.selectedTill.getTime())
                        $dayCell.addClass('selected');

                    if (this.options.minDate && date.getTime() <= this.options.minDate.getTime())
                        $dayCell.addClass('disabled');

                    if (this.options.maxDate && date.getTime() >= this.options.maxDate.getTime())
                        $dayCell.addClass('disabled');

                    $dayCell.addClass(this.getMonthClass(this.calendar.viewDate, date));

                    dayOffset++;
                }

                // Disable week
                if ($weekRow.find('.day-cell.disabled').length)
                    $weekCell.addClass('disabled');

                // Select week
                if ($weekRow.find('.day-cell.selected').length)
                    $weekCell.addClass('selected');
            }

            // Update months view
            var month = this.calendar.viewDate.getMonth();
            for (var m = 0; m < 12; m++)
            {
                var $monthCell = this.$monthsView.find('.month-' + m);
                if (m == month)
                    $monthCell.addClass('selected');
            }

            // Update years view
            var year = this.calendar.viewDate.getFullYear();
            for (var pos = 0; pos < 9; pos++)
            {
                var y = year + pos - 4;
                var $yearCell = this.$yearsView.find('.year-' + pos);
                $yearCell.find('span').text(y);
                $yearCell.data('year', y);

                if (year == y)
                    $yearCell.addClass('selected');
            }

            this.$header.find('.controls .action-year span').text(this.calendar.viewDate.getFullYear());
            this.$header.find('.controls .action-month span').text(this.options.locale.months[this.calendar.viewDate.getMonth()]);
        };
    };

    $.fn.DatePicker = function(options)
    {
        var args = arguments;
        return $(this).each(function()
        {
            var datePicker = $(this).data('DatePicker');

            if (datePicker != undefined && typeof datePicker[options] == 'function') {
               return datePicker[options].apply(datePicker, Array.prototype.slice.call( args, 1 ));
            }
            else if (typeof options === 'object' || ! options ) {
                datePicker = new DatePicker(this, options);
                datePicker.init();
                $(this).data('DatePicker', datePicker);
                return datePicker;
            }
            else {
                $.error('Method ' + options + ' does not exist on jQuery.DatePicker');
            }
        });
    };
})(window, jQuery);
